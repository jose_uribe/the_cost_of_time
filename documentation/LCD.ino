#include <LiquidCrystal_I2C.h> 
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int analogPin = A0;    // pin that the sensor is attached to
const int ledPin = 13;       // pin that the LED is attached to
const int threshold = 400;   // an arbitrary threshold level that's in the range of the analog input
int count_value =0;

void setup() {
  lcd.init(); // initialize the lcd
  lcd.backlight();


  pinMode(A4, OUTPUT);
  // initialize serial communications:
  Serial.begin(9600);
}

void loop() {
  // read the value of the potentiometer:
  int analogValue = analogRead(analogPin);

  // if the analog value is high enough, turn on the LED:
  if (analogValue < threshold) {
    count_value++;
    lcd.clear();
    lcd.setCursor(6, 0);
    lcd.print("Drops");
    lcd.setCursor(8, 1);
    lcd.print (count_value);
    delay(500);
  } else {
    lcd.setCursor(6, 0);
    lcd.print("Drops");
  }

  // print the analog value:
  Serial.println(analogValue);
  //delay(1);        // delay in between reads for stability 
  }
